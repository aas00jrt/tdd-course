import unittest
params=[0,1]

class FibenacciTests(unittest.TestCase):

    def test_firstTwoSameAsIndex(self):
        for i in params:
            self.assertEqual(i, self.calculate(i))

    def test_thirdNumberIsOne(self):
        self.assertEqual(1, self.calculate(2))

    def test_fourthNumberIsTwo(self):
        self.assertEqual(2, self.calculate(3))

    # def calculate(self, index):
    #     if index<2:
    #         return index
    #     else:
    #         return ((index-1) + (index-2))

    def calculate(self, index):
        a, b = 0, 1
        for i in range(0, index):
            a, b = b, a + b
        return a

if __name__ == '__main__':
    unittest.main()