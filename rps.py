import unittest
from parameterized import parameterized



class TestRPS(unittest.TestCase):
    @parameterized.expand([
        ("Rock", "Scissors", "Rock"),
        ("Rock", "Paper", "Paper"),
        ("Paper", "Rock", "Paper"),
        ("Paper", "Scissors", "Scissors"),
        ("Scissors", "Paper", "Scissors"),
        ("Scissors", "Rock", "Rock"),
        ("Scissors","Scissors","Draw"),
        ("Rock", "Rock", "Draw"),
        ("Paper", "Paper", "Draw")
    ])
    def test_Scissors1Wins(self,choice1,choice2,expected):
        winner = self.chooseWinner(choice1, choice2)
        self.assertEqual(winner , expected)

    def chooseWinner(self, choice1, choice2):
        choice1beats = Choice(choice1).beats()
        choice2beats = Choice(choice2).beats()
        if choice1beats == choice2:
            winner = choice1
        if choice2beats == choice1:
            winner = choice2
        if choice1 == choice2:
            winner = "Draw"
        return winner

class Choice():
    def __init__(self, actual):
        self.actual = actual

    def beats(self):
        if self.actual == "Rock":
            return "Scissors"
        if self.actual == "Paper":
            return "Rock"
        if self.actual == "Scissors":
            return "Paper"


if __name__ == '__main__':
    unittest.main()
